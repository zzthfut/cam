import vtk
from SliceAlgo import slice_topo, writeSlcFile

from StlModel import StlModel

modelName = "/home/zzt/Downloads/tower"
layerThk = 2
src = vtk.vtkSTLReader()
src.SetFileName(modelName + ".STL")
stlModel = StlModel()
stlModel.extractFromVtkStlReader(src)
layers = slice_topo(stlModel, layerThk)
writeSlcFile(layers, modelName + ".SLC")
print("SLC file has been generated and saved\n")
