from GeomBase import Point3D
from Segment import Segment
import random
import math


def makeMessySegs(circleNum=10, segNumPerCircle=100000, radius=100):
    segs = []
    r = radius
    for i in range(circleNum):
        pnts = []
        for j in range(segNumPerCircle):
            theta = j / segNumPerCircle * 2 * math.pi
            x = r * math.cos(theta)
            y = r * math.sin(theta)
            pnt = Point3D(x, y)
            pnts.append(pnt)
        pnts.append(pnts[0])
        for j in range(len(pnts) - 1):
            seg = Segment(pnts[j], pnts[j + 1])
            segs.append(seg)
        r += 10
    length = len(segs)
    print("segment count:", length)
    print("min segment length:", segs[0].A.distance(segs[0].B))
    for i in range(length):
        rd = random.randint(0, length - 1)
        segs[i], segs[rd] = segs[rd], segs[i]
    return segs
