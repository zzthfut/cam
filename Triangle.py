from GeomBase import Vector3D


class Triangle:
    def __init__(self, A, B, C, N=Vector3D(0, 0, 1)):
        self.A, self.B, self.C, self.N = A.clone(), B.clone(), C.clone(), N.clone()
        self.zs = []

    def __str__(self):
        print("Triangle:\n A:%s,B:%s,C:%s,N:%s\n" % (self.A, self.B, self.C, self.N))

    def zMinPnt(self):
        return min([self.A.z, self.B.z, self.C.z])

    def zMaxPnt(self):
        return max([self.A.z, self.B.z, self.C.z])
