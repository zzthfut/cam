import sys

sys.path.append("../")

from GeomBase import Point3D
from Polyline import Polyline
from VtkAdaptor import *

if __name__ == "__main__":
    VtkAdaptor = VtkAdaptor()
    VtkAdaptor.SetBackgroundColor(0.95, 0.95, 0.95)
    VtkAdaptor.drawAxes()
    VtkAdaptor.drawPoint(Point3D(10, 10, 10)).GetProperty().SetColor(1, 0, 0)
    VtkAdaptor.drawPoint(Point3D(50, 50, 50)).GetProperty().SetColor(1, 0, 0)
    polyline = Polyline()
    polyline.addPoint(Point3D(1, 1, 1))
    polyline.addPoint(Point3D(50, 2, 10))
    polyline.addPoint(Point3D(20, 10, 30))
    polyline.addPoint(Point3D(50, 80, 55))
    polylineActor = VtkAdaptor.drawPolyline(polyline)
    polylineActor.GetProperty().SetColor(0.1, 0.7, 0.7)
    polylineActor.GetProperty().SetLineWidth(2)
    stlActor = VtkAdaptor.drawStlModel("/home/zzt/Downloads/leng.STL")
    stlActor.SetPosition(0, 0, 90)
    VtkAdaptor.display()
