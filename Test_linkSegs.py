import time
from SliceAlgo import linkSegs_brutal, linkSegs_dlook, linkSegs_dorder
from makeMessySeg import makeMessySegs


segs = makeMessySegs(10, 1000)


start = time.perf_counter()
contours1 = linkSegs_dorder(segs)
end = time.perf_counter()
print("dorder: %f CPU seconds" % (end - start))

start = time.perf_counter()
contours2 = linkSegs_dlook(segs)
end = time.perf_counter()
print("dlook: %f CPU seconds" % (end - start))

start = time.perf_counter()
contours3 = linkSegs_brutal(segs)
end = time.perf_counter()
print("brutal: %f CPU seconds" % (end - start))
