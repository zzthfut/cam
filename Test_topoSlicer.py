import time
import vtk
from SliceAlgo import slice_combine, slice_topo

from StlModel import StlModel


stlModel = StlModel()
vtkreader = vtk.vtkSTLReader()
vtkreader.SetFileName("/home/zzt/Downloads/leng1.STL")
stlModel.extractFromVtkStlReader(vtkreader)
layerThks = [1, 0.5, 0.1, 0.05]
for layerThk in layerThks:
    print("layerThk:", layerThk)
    start = time.perf_counter()
    layers1 = slice_topo(stlModel, layerThk)
    end = time.perf_counter()
    print("slice_topo: %f seconds" % (end - start))
    start = time.perf_counter()
    layers2 = slice_combine(stlModel, layerThk)
    end = time.perf_counter()
    print("slice_combine: %f seconds" % (end - start))
