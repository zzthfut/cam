from ClipperAdaptor import ClipperAdaptor
import vtk
from GenHatch import genClipHatches, genSweepHatches
import Utility
from VtkAdaptor import VtkAdaptor


va = VtkAdaptor()
path = [(0, 0), (100, 0), (100, 100), (50, 60), (0, 100)]
poly = ClipperAdaptor(0).toPoly(path)
angle = Utility.degToRad(90)
# segs = genClipHatches([poly], 6, angle)
segs = genSweepHatches([poly], 6, angle)
va.drawPolyline(poly).GetProperty().SetColor(0, 0, 0)
for i in range(len(segs)):
    va.drawSegment(segs[i]).GetProperty().SetColor(1, 0, 0)
    textSrc = vtk.vtkVectorText()
    textSrc.SetText("%d" % (i))
    textActor = va.drawPdSrc(textSrc)
    textActor.SetPosition(segs[i].B.x, segs[i].B.y, segs[i].B.z)
    textActor.SetScale(3)
    textActor.GetProperty().SetColor(0, 0, 1)
va.display()
