from SliceAlgo import intersectStl_brutal, intersectStl_sweep, intersectStl_match
from StlModel import StlModel
import vtk
import time

if __name__ == "__main__":
    stlModel = StlModel()
    vtkreader = vtk.vtkSTLReader()
    vtkreader.SetFileName("/home/zzt/Downloads/leng1.STL")
    stlModel.extractFromVtkStlReader(vtkreader)
    print("face count:", stlModel.getFacetNumber())
    layerThks = [1, 0.2, 0.1]
    for layerThk in layerThks:
        print("layerThk:", layerThk)
        start = time.perf_counter()
        layer1 = intersectStl_sweep(stlModel, layerThk)
        end = time.perf_counter()
        print("sweep:%f CPU seconds" % (end - start))
        start = time.perf_counter()
        layer2 = intersectStl_match(stlModel, layerThk)
        end = time.perf_counter()
        print("match:%f CPU seconds" % (end - start))
        start = time.perf_counter()
        layer3 = intersectStl_brutal(stlModel, layerThk)
        end = time.perf_counter()
        print("brutal:%f CPU seconds" % (end - start))
        print("layer count:", len(layer1), "\n")
