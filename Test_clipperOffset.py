import pyclipper

from VtkAdaptor import VtkAdaptor
from GeomBase import Point3D
from Polyline import Polyline


def tuplesToPoly(tuples):
    poly = Polyline()
    for pt in tuples:
        poly.addPoint(Point3D(pt[0], pt[1], 0))
    poly.addPoint(poly.startPoint())
    return poly


outerPoly = [(0, 0), (100, 0), (100, 100), (0, 100)]
innerPoly = [(30, 30), (30, 70), (70, 70), (70, 30)]
pco = pyclipper.PyclipperOffset()
pco.AddPath(outerPoly, pyclipper.JT_ROUND, pyclipper.ET_CLOSEDPOLYGON)
pco.AddPath(innerPoly, pyclipper.JT_ROUND, pyclipper.ET_CLOSEDPOLYGON)
sln = pco.Execute(-7)
vtkAdaptor = VtkAdaptor()
for tuples in sln:
    poly = tuplesToPoly(tuples)
    vtkAdaptor.drawPolyline(poly).GetProperty().SetColor(0, 0, 0)
vtkAdaptor.display()
