import math


def makeListLinear(lists):
    outList = []
    _makeListLinear(lists, outList)
    return outList


def _makeListLinear(inList, outList):
    for a in inList:
        if not isinstance(a, list):
            outList.append(a)
        else:
            _makeListLinear(a, outList)


def degToRad(deg):
    return deg / 180 * math.pi


def radToDeg(rad):
    return rad / math.pi * 180
