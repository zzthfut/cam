import time
from GenDpPath import genDpPath
from SliceAlgo import readSlcFile
from Utility import degToRad
from VtkAdaptor import VtkAdaptor


if __name__ == "__main__":
    interval = 5
    angle = 0
    pathses = []
    layers = readSlcFile("/home/zzt/Downloads/tower.SLC")
    start = time.perf_counter()
    for i in range(len(layers)):
        print("dp,layer:%d/%d" % (i + 1, len(layers)))
        theta = degToRad(angle) if (i % 2 == 1) else degToRad(angle + 90)
        paths = genDpPath(layers[i].contours, interval, theta)
        pathses.append(paths)
    end = time.perf_counter()
    print("GenDpPath time:%f seconds" % (end - start))
    va = VtkAdaptor()
    for layer in layers:
        for contour in layer.contours:
            va.drawPolyline(contour).GetProperty().SetColor(0, 0, 0)
    for paths in pathses:
        for path in paths:
            va.drawPolyline(path).GetProperty().SetColor(1, 0, 0)
    va.display()
