import time
from GenCpPath import genCpPath
from SliceAlgo import readSlcFile
from VtkAdaptor import VtkAdaptor


interval = 0.2
shellThk = 1
pathses = []
modelName = "/home/zzt/Downloads/leng1.SLC"
layers = readSlcFile(modelName)
start = time.perf_counter()
for i in range(len(layers)):
    print("cp,layer:%d/%d" % (i + 1, len(layers)))
    paths = genCpPath(layers[i].contours, interval, shellThk)
    pathses.append(paths)
end = time.perf_counter()
print("GenCpPath time:%f CPU seconds" % (end - start))
va = VtkAdaptor()
for layer in layers:
    for contour in layer.contours:
        va.drawPolyline(contour).GetProperty().SetColor(0, 1, 0)
for paths in pathses:
    for path in paths:
        va.drawPolyline(path).GetProperty().SetColor(1, 0, 0)
va.display()
