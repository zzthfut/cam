from GeomBase import Point3D


class LinkPoint:
    def __init__(self, pnt3d: Point3D, digits=7):
        self.x = round(pnt3d.x, digits)
        self.y = round(pnt3d.y, digits)
        self.z = round(pnt3d.z, digits)
        self.other = None
        self.used = False
        self.index = 0

    def __str__(self):
        return (
            "LinkPoint: used:%s\n self(%s,%s,%s)\n other(%s,%s,%s)\n linked to %s"
            % (
                self.used,
                self.x,
                self.y,
                self.z,
                self.other.x,
                self.other.y,
                self.other.z,
                self.other.index,
            )
        )

    def toPoint3D(self):
        return Point3D(self.x, self.y, self.z)
