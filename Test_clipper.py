from GeomBase import Point3D
import pyclipper
from VtkAdaptor import VtkAdaptor
from pyclipper import Pyclipper
from Polyline import Polyline


def tuplesToPoly(tuples):
    poly = Polyline()
    for pt in tuples:
        poly.addPoint(Point3D(pt[0], pt[1], 0))
    poly.addPoint(poly.startPoint())
    return poly


if __name__ == "__main__":
    subject = [(0, 0), (100, 0), (100, 70), (0, 70)]
    clip = [(30, 50), (70, 50), (70, 100), (30, 100)]
    clipper = Pyclipper()
    clipper.AddPath(subject, pyclipper.PT_SUBJECT, True)
    clipper.AddPath(clip, pyclipper.PT_CLIP, True)
    sln = clipper.Execute(pyclipper.CT_UNION)
    vtkAdaptor = VtkAdaptor()
    for tuples in sln:
        poly = tuplesToPoly(tuples)
        vtkAdaptor.drawPolyline(poly).GetProperty().SetColor(0, 0, 0)
    vtkAdaptor.display()
